
# Wikimedia 

<hr>

Jonathan Gabriel Moreno Arce<br>
20190128<br>
jonathan__gabriel@hotmail.com

<hr>

## Description
<p> This project is intended to monitor, store and visualize the Recent Change event stream fof Wikipedia as a <b><i>proof of concept</i></b>.</p>

## Scripts:
<ul>
    <li>Wikimedia.py</li>
    <li>GetImage.py</li>
</ul>

## General Overview
<p> This project contains three main components: the scripts (<b>Wikimedia.py and GetImage.py</b>), the database (<b>MariaDB</b>) and the web server (<b>Django</b>). The Wikimedia.py script is in charge of processing the events and storing the messages into the MariaDB Database. The GetImage.py script is in charge of generating a png image which will be displayed in the web site. This script is called from the web server each time the page is refreshed. </p>

## Scope
<p> This project is a proof of concept. No further requirements are present. Being a proof of concept the desing and implementation was done very fast since the goal is to show funcionality only.

## Architecture Design

The following image shows the components used in this implementation. 
<img src="./images/design.jpg" alt="Image Design">

## Considerations
<ul>
    <li>Storage is limited so there is not a need of huge storage nor processing power.</li>
    <li>There are not operations done in the data. The data flows straight forward so processing power is low. No intensive processing required. </li>
    <li>The system is limited to store 1000 records and to visualize the changes per second in an image through a web page.</li>
    <li>The web page is not interactive so far. </li>
    <li>The data pipeline is very simple since no transformation is required so no more data processing or desing or mapping is required. </li>
</ul>  

## Technology Stack
<ul>
    <li><b>Python</b> language was chosen as it is a business requirement. Python is an interpreted language, high-level, general-purpose programming language. Python is readable and easy to implement. </li>
    <li><b>MariaDB</b> was chosen as it is simple and has good performance being an open source database. MariaDB is based on MySQL Database. JSON check constraint is available so when inserting json file this is validated in the database as well which allows double check on JSON document. For this project, the developer had a running installation on a virtual machine so MariaDB was used due to time limitations as well. No installation required during the development of this project.</li>
    <li><b>Django</b> is a high-level Python Web framework that encourages rapid development and clean, pragmatic design. Django is fast, secure, tested and scalable so I included it as web server. It is easy to deploy and configure. In no more than two hours it can be up and running while for experienced developers it can be done in few minutes. 
</ul>  

#### Alternatives
<ul>
    <li><b>Apache Spark</b> is an open-source distributed general-purpose cluster computer framework. Apache Spark is a unified analytics engine for big data processing with built-in modules for streaming, SQL, machine learning and graph processing. Dataframes are available so that data processing could be done with <b>Pandas</b> which is very common in Python data processing data pipelines. </li>
    <li>If the data required futher processing I definitely would used Spark as a main framework. Spark is integrated with Python and it has a lot of tool than can be used within Python for streaming and data processing. Besides, there is a data pipelines framework that allows us to manage the data lineage easily. </li>
    <li>Regarding scalability, with Spark it would be very easy to deploy code into a cluster for futher data processing power. In this project, I did not consider Spark due to the lack of data processing stage. There was no need for data processing power. </li>
    <li>Regarding data pipelines, there are a lot of tools that could be used like <b>Luigi</b> or <b>Apache AirFlow</b> or we could search in the internet for other tools based on directed acyclic graphs (DAGs) to manage the data pipeline. </li>
</ul>

## Methodology
<p>I used a combination of spiral and extremme programming to build a small part of the code of each block incrementally. I started with mocking the Event Processing, desingning the database and set up the Djagon server. Then in a second round I connected Python with MySQL and monitored the database. In the web page side, I started creating objects and modifying parameters so that my code could run. And so on, at the end, I got most of the blocks up and running. </p>

## Test Results

<img src="./images/WikimediaConsole.jpg" alt="Image Design">

<img src="./images/WikimediaLog.jpg" alt="Image Design">

<img src="./images/WikimediaSite.jpg" alt="Image Design">

## Improvements

There are some basic improvements to my design:
<ul>
    <li>Configuration Files for database and source paths.</li>
    <li>SQL Statements parametrized. External files must be considered.</li>
    <li>Considering to include indexes in the database would be a good idea.</li>
    <li>A view may be created to show the data and only call the view from the site</li>
    <li>Improve Exception handling to catch specific errors and manage them in order to take actions.</li>
    <li>Consider sending emails on failure or process abnormal termination.</li>
    <li>On the web page, the image can be generated internally with any framework in order to be displayed as an element in the html instead of an image. In my design the image is generated by calling a script from the view, then the image is placed in one folder and then displayed in the page. I did not follow any design pattern to achieve this. </li>
    <li>MVC architecture must be applied in the web site design to follow Django architecture.</li>
</ul>
    
