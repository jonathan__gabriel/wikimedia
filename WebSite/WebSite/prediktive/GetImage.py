
# coding: utf-8

# <hr>
#     File Name: GetImage.py <br>
#     Author: Jonathan Gabriel Moreno Arce <br>
#     Date Created: 20190127 <br>
#     Python Version: 3.6 <br>
# <hr>
# <h2> Summary </h2>
# This script is in charge of creating plot.png image after reading data from the MySQL database when the web page is refreshed. <br>
# This script contains three functions & the main:
# <ul>
#   <li>configLog()</li>
#   <li>getData() : DataFrame</li>
#   <li>saveImage(DataFrame) </li>
# </ul>

# In[1]:


## Imports 
import pymysql # To manage MySQL connections and dabase processes.
import json    # To read and to process JSON Documents
import logging # To manage logging
import logging.config # To setup the log
import pandas as pd   # To process data as dataframe
#import pandas.io.sql as psql 
import matplotlib.pyplot as plt # To plot data
import os # To remove file before creation plot.png
import datetime # To get date 



# In[2]:


# configLog() funtion reads the json config file to setup the log
def configLog():
    pathLogFile = "./config/configLog.json"
    with open(pathLogFile, 'rt') as f:
        config = json.load(f)
    logging.config.dictConfig(config)

    logger = logging.getLogger("loggers")


# In[3]:


# getData() connects to the MySQL DB and execute the query to retrieve the report, this function returns a dataframe with the results.
def getData():
    conn = pymysql.connect(host = '192.168.1.90', user = 'jonathan', password = 'dev123', port = 3306, 
                       cursorclass=pymysql.cursors.DictCursor,
                       charset="utf8")
    try:
        sql = "select second(d) min, count(*) changes from wikimedia.data group by 1 order by 1 asc"
        df = pd.read_sql(sql, conn, columns=['min', 'changes'])
    except Exception as e:
        print(e)
    finally:
        conn.close()
    return df


# In[4]:


# saveImage(df) receives a DataFrame as input and generate the plot.png image in the path shown. 
# If the file exists in the path it is deleted since the savefig() function does not overwrite the file.
def saveImage(df):
    #fileName = './WebSite/WebSite/prediktive/static/plot.png'
    fileName = "./prediktive/static/plot.png"
    if(os.path.isfile(fileName)):
        os.remove(fileName)
        print("removed")
        
    df['changes'].plot(x=df['min'], title='Changes per minute - ' + str(datetime.datetime.now()), grid=True, colormap=plt.get_cmap('summer'))
    plt.xlabel('time')
    plt.ylabel('Changes')
    plt.savefig(fileName)
    plt.close()
    del df


# In[5]:


# main executes the functions in the correct order.
def main():
    #configLog()
    df = getData()
    saveImage(df)
    del df


# In[6]:


# main is called from the outside of the script
if __name__ == "__main__":
    main()

