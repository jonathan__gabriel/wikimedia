from django.db import models

from django.core.files.storage import FileSystemStorage
from django.conf import settings

# Create your models here.

image_storage = FileSystemStorage(
    # Physical file location ROOT
    location=u'{0}/prediktive/'.format(settings.MEDIA_ROOT),
    # Url for file
    base_url=u'{0}prediktive/'.format(settings.MEDIA_URL),
)


def image_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/my_sell/picture/<filename>
    return u'images/{}'.format(filename)


class Goods(models.Model):
    pic = models.ImageField(upload_to=image_directory_path, storage=image_storage)