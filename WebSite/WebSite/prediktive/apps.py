from django.apps import AppConfig


class PrediktiveConfig(AppConfig):
    name = 'prediktive'
